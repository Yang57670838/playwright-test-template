import * as dotenv from "dotenv";

export const getEnv = () => {
  dotenv.config({
    path: `.env.${process.env.NODE_ENV}`,
  });
};
