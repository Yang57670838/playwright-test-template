import { Given, When, Then, setDefaultTimeout } from "@cucumber/cucumber";
import { expect } from "@playwright/test";

setDefaultTimeout(60 * 1000 * 2);

Given("I am on the main page", async function () {
  await this.loginPage.goTo(process.env.PAGE_URL);
  // await pageObject.page.waitForTimeout(1000)
});

Given("I enter the admin username", async function () {
  await this.loginPage.enterUserName(process.env.ADMIN_USERNAME);
  // await page.pause()
});

Given("I enter the admin password", async function () {
  await this.loginPage.enterPassword(process.env.ADMIN_PASSWORD);
});

When("I click on the login button", async function () {
  await this.loginPage.login();
});

Then("I see the login fail message", async function () {
  await expect(this.page.locator("#error-element-password")).toBeVisible();
});

When(/^I enter the password "([^"]*)"$/, async function (password: string) {
  await this.loginPage.enterPassword(password);
});

When(/^I enter the username "([^"]*)"$/, async function (username: string) {
  await this.loginPage.enterUserName(username);
});

Then(
  /^I "([^"]*)" see dashboard page heading$/,
  async function (value: string) {
    if (value === "should not") {
      await expect(
        this.page.locator('h1:has-text("Accounting DashBoard")')
      ).not.toBeVisible();
    } else if (value === "should") {
      await expect(
        this.page.locator('h1:has-text("Accounting DashBoard")')
      ).toBeVisible();
    } else {
      throw new Error("Asserting wrong typo");
    }
  }
);
