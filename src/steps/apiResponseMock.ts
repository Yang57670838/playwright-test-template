import { Given, When, Then, setDefaultTimeout } from "@cucumber/cucumber";
import { chromium, Page, Browser, expect, Route } from "@playwright/test";

setDefaultTimeout(60 * 1000 * 2); // 2 mins

Given("I mock API response for fetch analyse", async function () {
  await this.page.route(/\/api\/company\/.*\/analyst/, (route: Route) => {
    const response = {
      data: {
        number: 1111111,
      },
    };
    route.fulfill({
      status: 200,
      body: JSON.stringify(response),
    });
  });
});

Then(
  "I expect a request to get existing submit data will give success response",
  async function () {
    await this.page.route("**/bff/submittedData/**", (route: Route) => {
      route.fulfill({
        status: 200,
        body: JSON.stringify([{ title: "Mocked Post" }]),
      });
    });
  }
);
