import { Given, When, Then, setDefaultTimeout } from "@cucumber/cucumber";
import { chromium, Page, Browser, expect, Route } from "@playwright/test";
import { ifElementShowAfterTime } from "../utils/common";

setDefaultTimeout(60 * 1000 * 2);

When(
  "I select the report period to option index {int}",
  async function (optionIndex) {
    await this.dashboardPage.selectPeriod(optionIndex);
  }
);

Then("I see welcome greeting message in dashboard", async function () {
  const title = this.dashboardPage.h1;
  await expect(title).toContainText(`Accounting DashBoard`);
});

Then("I see dashboard page title", async function () {
  const title = this.dashboardPage.h1;
  await expect(title).toContainText(`Accounting DashBoard`);
  const isAlertExistingInDashboardLanding = await ifElementShowAfterTime(
    this.page,
    '[data-testid="alertMessageBox_dashboardAlertMessage"]',
    10000
  );
  if (isAlertExistingInDashboardLanding) {
    console.log("There is alert!!!");
  } else {
    console.log("There is no alert currently!!!");
  }
});

Given("I am on the engagements page", async function () {
  await this.dashboardPage.goToEngagementPageByLeftMenu();
});
