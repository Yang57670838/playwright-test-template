import { Given, When, Then, setDefaultTimeout } from "@cucumber/cucumber";
import { chromium, Page, Browser, expect } from "@playwright/test";
import { locatorMapper } from "../utils/locatorMapper";

setDefaultTimeout(60 * 1000 * 2);

// find a select dropdown's selected option's label (not value)
// for example in feature file: Then I should see the select "mockSelect" has selected option with label "Option 1"
Then(
  /^I should see the select "([^"]*)" has selected option with label "([^"]*)"$/,
  async function (selectElement: string, label: string) {
    const selector = locatorMapper[selectElement];
    const selectLabel = await this.page
      .locator(selector)
      .evaluate((element: any) => element.options[element.selectedIndex].text);
    expect(selectLabel).toEqual(label);
  }
);
