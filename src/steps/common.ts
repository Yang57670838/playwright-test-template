import { Given, When, Then, setDefaultTimeout } from "@cucumber/cucumber";
import { chromium, Page, Browser, expect } from "@playwright/test";

setDefaultTimeout(60 * 1000 * 2);

Then("I must be taken to dashboard page", async function () {
  const url = this.page.url();
  expect(url.includes(process.env.PAGE_URL)).toEqual(true);
});

Then("I pause the page for debug purpose", async function () {
  await this.page.pause();
});

When('I open a new tab with key {string}', async function (tabKey: string) {
  await this.openNewTab(Number(tabKey)); // Open a new tab
  console.log(`Opened a new tab with key: ${tabKey}`);
});

When('I navigate to the main page in tab {string}', async function (tabKey: string) {
  const page = this.pages[Number(tabKey)];
  if (!page) {
    throw new Error(`Tab with key ${tabKey} does not exist`);
  }
  await page.goto(process.env.PAGE_URL);
  console.log(`Navigated to ${process.env.PAGE_URL} in tab ${tabKey}`);
});

Given('I set a value in the World object', function () {
  this.set('greeting', 'Hello, Cucumber!');
  console.log('Value set in the World object.');
});

// just for test customized world object can share state successfully acros steps in same scenario
Then('I retrieve the value from the World object', function () {
  const value = this.get('greeting');
  console.log('Retrieved value.........', value);
  if (value !== 'Hello, Cucumber!') {
    throw new Error(`Expected 'Hello, Cucumber!' but got '${value}'`);
  }
});
