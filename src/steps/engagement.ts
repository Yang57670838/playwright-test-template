import { Given, When, Then, setDefaultTimeout } from "@cucumber/cucumber";
import { chromium, Page, Browser, expect, Route } from "@playwright/test";
import { ifElementShowAfterTime } from "../utils/common";

setDefaultTimeout(60 * 1000 * 2);

When("I select today as start date filter", async function () {
  await this.page.getByTestId("CalendarIcon").nth(0).click();
  await this.page.locator(".MuiPickersDay-today").nth(0).click();
});

Then("I can not select today as end date filter", async function () {
  const todayNum = new Date().getDate();
  console.log("todayNum", todayNum);
  await this.page.getByTestId("CalendarIcon").nth(1).click();
  const regex = new RegExp(`^${todayNum}$`)
  const todayBtn = this.page.locator("button", { hasText: regex });
  await expect(todayBtn).toBeDisabled();
});

