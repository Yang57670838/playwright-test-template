@dashboard
Feature: The Dashboard page

    Background:
        Given I am on the main page

    Scenario: Select period refresh dashboard records
        Given I enter the admin username
        And I enter the admin password
        And I mock API response for fetch analyse
        When I click on the login button
        Then I see dashboard page title
        # And I pause the page for debug purpose

    # Scenario Outline: As a user, I can log into the dashboard and see left menu bar

    #     Then I should see the menu item <menuIndex> with text <menuTxt>

    #     Examples:
    #         | menuIndex | menuTxt                     |
    #         | 0         | Summary                     |
    #         | 1         | Cash Expense                |
    #         | 2         | Company Expense             |
    #         | 3         | Employee Salary             |
    #         | 4         | Personal Creditcard Expense |
    #         | 5         | Income                      |
    #         | 6         | Bank Statement              |
    #         | 7         | Submit GST Report           |
    #         | 8         | Submit Extra Document       |
