@login
Feature: User Authentication tests

    Background:
        Given I am on the main page

    Scenario Outline: Login failed with expired user <username>
        When I enter the username "<username>"
        # And I set a value in the World object
        And I enter the password "<password>"
        And I click on the login button
        Then I "<shouldSee>" see dashboard page heading
        # When I open a new tab with key "tab1"
        # And I navigate to the main page in tab "tab1"
        # Then I retrieve the value from the World object
        # And I pause the page for debug purpose

        Examples:
            | username          | password | shouldSee  |
            | ly@hotmail.com    | 123      | should not |
            | yangliu@gmail.com | 123      | should not |

