import { Page } from "@playwright/test";

export const ifElementShowAfterTime = async (
  page: Page, // or iframe selector
  selector: string,
  timeout: number,
  isIframe = false
): Promise<boolean> => {
  let isShow: boolean;
  try {
    if (isIframe) {
      await page.locator(selector).waitFor({
        state: "visible",
        timeout: timeout,
      });
      isShow = true;
    } else {
      await page.waitForSelector(selector, {
        state: "visible",
        timeout: timeout,
      });
      isShow = true;
    }
  } catch (error) {
    isShow = false;
  }
  return isShow;
};
