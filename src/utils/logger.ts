import winston from "winston";

const consoleFormat = winston.format.printf(({ level, message }) => {
    const logLevel = winston.format.colorize().colorize(level, `${level.toUpperCase()}`)
    return `[${logLevel}]: ${message}`
})

// TODO: add some logs into S3 or google bigquery or Azure table.. or data dog.. or SNS email..with differnt transport..
let logger = winston.createLogger({
    transports: [
        new winston.transports.Console({
            level: "debug",
            handleExceptions: true,
            format: winston.format.combine(winston.format.timestamp(), consoleFormat)

        })
    ]
})

logger.on("error", error => {
    console.log("Unknown error in winston logger")
    console.log(error.message)
})

export default logger
