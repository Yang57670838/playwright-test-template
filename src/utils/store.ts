import { IWorld, setWorldConstructor, IWorldOptions } from "@cucumber/cucumber";
import { Browser, BrowserContext, Page, chromium } from "playwright";

interface SharedState {
  [key: string]: any; // Define a flexible key-value type for shared state
}

class CustomWorld implements IWorld {
  sharedState: SharedState; // Shared state for scenario
  browser!: Browser; // Playwright browser instance
  context!: BrowserContext; // Playwright browser context
  pages: Record<string, Page>; // Store multiple pages by keys
  attach: (data: any, mediaType?: string) => any; // Attach method for Cucumber
  log: (text: string) => void; // Log method for Cucumber
  parameters: { [key: string]: string }; // Retrieve scenario parameters

  constructor(options: IWorldOptions<{ [key: string]: string }>) {
    this.sharedState = {};
    this.pages = {};
    this.log = () => {}; // Default implementation for log
    this.parameters = options.parameters; // Retrieve parameters
    this.attach = options.attach; // Attach method for Cucumber
  }

  // Initialize a new Playwright browser with opening a new tab in it
  async initializeNewBrowser(tabKey: number): Promise<Page> {
    this.browser = await chromium.launch({
      headless: process.env.ISHEADLESS === "true",
      slowMo: Number(process.env.SLOWMO) || 0,
    });
    const contextOptions = {
      recordVideo: process.env.ENABLE_VIDEO
        ? {
            dir: "test-results/", // default Directory to save videos
          }
        : undefined, // Don't enable video if the variable is not true
    };
    this.context = await this.browser.newContext(contextOptions);
    const page = await this.context.newPage();
    this.pages[tabKey] = page; // Store the new tab with a key
    return page;
  }

  // open a new browser tab in the initialized browser context
  async openNewTab(tabKey: number): Promise<Page> {
    const page = await this.context.newPage();
    this.pages[tabKey] = page; // Store the new tab with a key
    return page;
  }

  // Close Playwright browser
  async closeBrowser(): Promise<void> {
    if (this.context) {
      await this.context.close();
    }
    if (this.browser) {
      await this.browser.close();
    }
  }

  // Set a value in the shared state
  set(key: string, value: any): void {
    this.sharedState[key] = value;
  }

  // Get a value from the shared state
  get<T>(key: string): T | undefined {
    return this.sharedState[key] as T;
  }
}

setWorldConstructor(CustomWorld);
