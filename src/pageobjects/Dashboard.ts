import { Locator, Page } from "@playwright/test";
import { BasePage } from "./BasePage";

export class DashboardPage extends BasePage {
  readonly periodSelect: Locator;
  readonly engagementLeftMenuBtn: Locator;

  constructor(page: Page) {
    super(page);
    this.periodSelect = page.locator("#periodSelect");
    this.engagementLeftMenuBtn = page.locator("#engagements");
  }

  async selectPeriod(optionIndex: number) {
    await this.periodSelect.click();
    const option = this.page.locator(`#periodSelect-option-${optionIndex}`);
    await option.click();
  }

  async goToEngagementPageByLeftMenu() {
    await this.engagementLeftMenuBtn.click();
  }
}
