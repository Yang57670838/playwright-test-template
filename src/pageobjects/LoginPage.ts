import { Locator, Page } from '@playwright/test';
import { BasePage } from "./BasePage";

export class LoginPage extends BasePage {
    readonly signinBtn: Locator;
    readonly emailInput: Locator;
    readonly pdInput: Locator;

    constructor(page: Page) {
        super(page);
        this.signinBtn = page.locator("[type='submit']");
        this.emailInput = page.locator('#email');
        this.pdInput = page.locator('#password');
    }

    async goTo(url: string) {
        await this.page.goto(url)
    }

    async enterUserName(username: string) {
        await this.emailInput.fill(username)
    }

    async enterPassword(password: string) {
        await this.pdInput.fill(password)
    }

    async login() {
        await this.signinBtn.click()
    }
}
