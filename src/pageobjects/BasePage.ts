import { Locator, Page } from '@playwright/test';

export class BasePage {
    readonly page: Page;
    readonly h1: Locator;

    constructor(page: Page) {
        this.page = page;
        this.h1 = page.locator('h1');
    }
}
