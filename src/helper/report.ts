const report = require("multiple-cucumber-html-reporter");

report.generate({
  jsonDir: "test-results",
  reportPath: "./",
  reportName: "playwright-test-template-report",
  pageTitle: "Accounting Template Auto Testing Report"
});