import { BeforeAll, AfterAll, Before, After, Status } from "@cucumber/cucumber";
import {
  chromium,
  Page,
  Browser,
  BrowserContext,
  Route,
  Request,
  Response,
} from "@playwright/test";
const fs = require("fs");
import path from "path";
import { LoginPage } from "../pageobjects/LoginPage";
import { DashboardPage } from "../pageobjects/Dashboard";
import { getEnv } from "../env/env";
import logger from "../utils/logger";

// let browser: Browser;
// let context: BrowserContext;
let page: Page;

BeforeAll(async function () {
  getEnv();
});

Before(async function () {
  //   browser = await chromium.launch({
  //     headless: process.env.ISHEADLESS === "true",
  //     slowMo: Number(process.env.SLOWMO) || 0,
  //   });
  //   const contextOptions = {
  //     recordVideo: process.env.ENABLE_VIDEO
  //       ? {
  //           dir: 'test-results/', // default Directory to save videos
  //         }
  //       : undefined, // Don't enable video if the variable is not true
  //   };
  //   context = await browser.newContext(contextOptions);
  page = await this.initializeNewBrowser(0);
  this.page = page;
  this.loginPage = new LoginPage(page);
  this.dashboardPage = new DashboardPage(page);
  // logging request
  // this.page.on("request", (request: Request) => console.log("requesting: ", request.url()));
  this.page.on("response", (response: Response) =>
    logger.info(`${response.url()}..${response.status()}`)
  );
  // TODO: will enable it in full production test for screenshot purpose, by env variable
  await this.page.route("**/*", (route: Route) => {
    return route.request().resourceType() === "image"
      ? route.abort()
      : route.continue();
  });
});

After(async function ({ result, pickle }) {
  const video = await this.page.video();
  if (result?.status === Status.FAILED) {
    // screenshot
    const img = await this.page.screenshot({
      path: `test-results/screenshots/${pickle.name}.png`,
      type: "png",
      timeout: 120000,
    });
    // attach to reporter
    await this.attach(img, "image/png");
    // failing video
    if (video) {
      const videoPath = await video.path();
      const videoName = `${pickle.name.replace(/ /g, "_")}_${
        result.status
      }.webm`;
      // Rename or move the video file
      if (!fs.existsSync("test-results/fail")) {
        fs.mkdirSync("test-results/fail", { recursive: true });
      }
      fs.renameSync(videoPath, `test-results/fail/${videoName}`);
      console.log(`Video saved for scenario: ${videoName}`);
      /// Attach the video file path to the report
      const fullPath = path.resolve(`test-results/fail/${videoName}`);
      this.attach(
        `<video controls style="max-width: 100%"><source src="${fullPath}" type="video/webm"></video>`,
        "text/html"
      );
    }
  }
  if (result?.status === Status.PASSED) {
    // success video
    if (video) {
      const videoPath = await video.path();
      const videoName = `${pickle.name.replace(/ /g, "_")}_${
        result.status
      }.webm`;
      // Rename or move the video file
      if (!fs.existsSync("test-results/success")) {
        fs.mkdirSync("test-results/success", { recursive: true });
      }
      fs.renameSync(videoPath, `test-results/success/${videoName}`);
      console.log(`Video saved for scenario: ${videoName}`);
      /// Attach the video file path to the report
      const fullPath = path.resolve(`test-results/success/${videoName}`);
      this.attach(
        `<video controls style="max-width: 100%"><source src="${fullPath}" type="video/webm"></video>`,
        "text/html"
      );
    }
  }
  await this.page.close();
  await this.closeBrowser();
});

// AfterAll(async function (this: any) {
//   await this.closeBrowser();
// });
