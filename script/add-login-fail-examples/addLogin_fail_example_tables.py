import os

# Define test data dynamically
test_data = [
    {"username": "user1", "password": "pass123"},
    {"username": "admin", "password": "adminPass"},
    {"username": "guest", "password": "guestPass"},
]

# Path to existing feature file
feature_file_path = "script/add-login-fail-examples/login.feature"

# Read the existing feature file
with open(feature_file_path, "r") as f:
    lines = f.readlines()


# Append `Examples` table after `Scenario Outline:`
examples_table = f"""
    Examples:
      | username  | password  |
      {chr(10).join(f'| {row["username"]} | {row["password"]} |      ' for row in test_data)}
"""

insert_index = 20
lines.insert(insert_index, examples_table)

# Write the updated feature file
with open(feature_file_path, "w") as f:
    f.writelines(lines)

print(f"✅ Successfully appended `Examples:` table to {feature_file_path}")