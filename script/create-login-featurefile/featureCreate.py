import io
import csv

def create_one_feature_file(filename, username, password):
    # Create an in-memory text buffer
    feature_file = io.StringIO()
    feature_content = f"""@login
    Feature: User Authentication tests

    Background:
        Given I am on the main page

    """
    scenario_content = f"""  Scenario: Login success
        Given I enter the {username}
        And I enter the {password}
        And I click on the login button
        Then I must be taken to dashboard page
    """
    feature_file.write(feature_content + scenario_content)
    with open(filename, "w") as file:
        file.write(feature_file.getvalue())  # Write everything at once
    # Close the memory buffer
    feature_file.close()

with open("./script/login-base.csv", "r") as file:
    reader = csv.reader(file)
    
    header = next(reader)
    print("Header:", header)
    for row in reader:
        print(row)
        create_one_feature_file(row[0], row[1], row[2])

print("✅ Large file written efficiently!")
