import { test, expect } from '@playwright/test';

test('has title', async ({ page }) => {
  await page.goto('https://playwright.dev/');

  // Expect a title "to contain" a substring.
  await expect(page).toHaveTitle(/Playwright/);
});

test('get started link', async ({ page }) => {
  await page.goto('https://playwright.dev/');

  // Click the get started link.
  await page.getByRole('link', { name: 'Get started' }).click();

  // Expects the URL to contain intro.
  await expect(page).toHaveURL(/.*intro/);
});

test('open page from browser instance', async ({ browser }) => {
  const context = await browser.newContext()
  const page = await context.newPage()
  await page.goto('http://localhost:3000')
});

test('open page directly with default browser', async ({ page }) => {
  await page.goto('http://localhost:3000')
  console.log(await page.title())
  expect(page).toHaveTitle("Google")
});

test('login fail', async ({ page }) => {
  await page.goto('http://localhost:3000/login')
  await page.locator('#username').type("random name")
  await page.locator('#password').type("random password")

  // click login btn
  await page.locator("form div button[type='submit']").nth(1).click()

  // find login fail text
  await expect(page.locator('#error-element-password')).toContainText("Wrong email or password")
});

test('login success', async ({ page }) => {
  await page.goto('http://localhost:3000/login')
  const userName = page.locator('#username');
  const password = page.locator('#password');
  const signinBtn = page.locator("form div button[type='submit']").nth(1)
  await userName.fill("correct name")
  await password.fill("correct password")

  // click login btn
  await signinBtn.click()

  // network wait until idle
  await page.waitForLoadState('networkidle');

});

test('select', async ({ page }) => {
  await page.goto('http://localhost:3000/submit-gst-report')
  
  const dropdown = page.locator("select")
  await dropdown.selectOption("value") // option value

  await page.pause()

});

test('checkbox', async ({ page }) => {
  await page.goto('http://localhost:3000/submit-gst-report')
  
  const checkbox = page.locator("input[type='checkbox']")
  await checkbox.click()
  await expect(checkbox).toBeChecked()

  // await checkbox.uncheck()
  // expect(await checkbox.isChecked()).toBeFalsy()

});

test('validate attribute', async ({ page }) => {
  const element = page.locator("..")
  await expect(element).toHaveAttribute("class", "specialName")
});
